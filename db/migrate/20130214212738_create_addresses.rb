class CreateAddresses < ActiveRecord::Migration
  def change
    create_table :addresses do |t|
      t.string :country
      t.string :city
      t.integer :zip_code
      t.string :street
      t.integer :user_info_id

      t.timestamps
    end
  end
end
