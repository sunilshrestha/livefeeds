class CreateUserInfos < ActiveRecord::Migration
  def change
    create_table :user_infos do |t|
      t.string :first_name
      t.string :last_name
      t.string :user_name
      t.string :profession
      t.integer :phone
      t.integer :user_id

      t.timestamps
    end
  end
end
