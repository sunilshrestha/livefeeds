class Interest < ActiveRecord::Base
  attr_accessible :title
  has_many :user_info
  has_many :user_info, :through=>:user_interests
end
