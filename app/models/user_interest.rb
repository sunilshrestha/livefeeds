class UserInterest < ActiveRecord::Base
  attr_accessible :interest_id, :user_info_id
  belongs_to :user_info
  belongs_to :interest
  
end
