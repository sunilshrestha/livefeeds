class Role < ActiveRecord::Base
  attr_accessible :description, :role_name
  has_many :users
  has_many :users,:through=>:user_roles
end
