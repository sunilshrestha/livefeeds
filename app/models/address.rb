class Address < ActiveRecord::Base
  attr_accessible :city, :country, :street, :user_info_id, :zip_code
  belongs_to :user_infos
end
