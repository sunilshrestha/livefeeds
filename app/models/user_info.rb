class UserInfo < ActiveRecord::Base
  attr_accessible :first_name, :last_name, :phone, :profession, :user_id, :user_name
 belongs_to :user
 has_one :address
 has_many :interests
 has_many :interests, :through=>:user_interests
 
end
